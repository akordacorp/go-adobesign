package main

import (
	"context"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/akordacorp/go-adobesign/adobesign/v6"
)

func main() {
	client := v6.NewClient("", "na4", "", "")

	agreement, err := client.AgreementService.GetAgreement(context.Background(), "CBJCHBCAABAA5ZX2_eVPtZs0F0nV5TdWAFEOY2ag7-Hj")
	if err != nil {
		return
	}

	file, err := os.Open("/Users/aesadde/Downloads/akorda_nda_test.pdf")
	defer file.Close()
	if err != nil {
		panic(err)
	}

	data, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatal(err)
	}

	document, err := client.TransientDocumentService.UploadTransientDocument(context.Background(), data, "akorda_nda_test.pdf")
	if err != nil {
		log.Fatal(err)
	}

	agreement.FileInfos = []v6.FileInfo{{TransientDocumentId: document.TransientDocumentId}}
	err = client.AgreementService.UpdateAgreement(context.Background(), *agreement)
	if err != nil {
		log.Fatal(err)
	}
}
