package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/akordacorp/go-adobesign/adobesign/v6"
)

func main() {
	client := v6.NewClient("3AAABLblqZhDJexum49xZDsQNOA4fmNA7scP77HObv_LpQ7qu8RIzY5zA46TfuRApdc2XXt9YEtatxpbYKdLd1r0V7Bi5iugG", "na4", "", "")

	file, err := os.Open("/Users/petefc/Downloads/test-sync-4-TNDA-2023-04-20.docx")
	//defer file.
	if err != nil {
		panic(err)
	}

	data, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatal(err)
	}

	document, err := client.TransientDocumentService.UploadTransientDocument(context.Background(), data, "TEST.docx")
	if err != nil {
		log.Fatal(err)
	}

	agreement, err := client.AgreementService.CreateAgreement(context.Background(), v6.Agreement{
		FileInfos: []v6.FileInfo{{TransientDocumentId: document.TransientDocumentId}},
		Name:      "test document",
		ParticipantSetsInfo: []v6.ParticipantSetInfo{{
			MemberInfos: []v6.MemberInfo{{Email: "test@test.com"}},
			Order:       1,                         // the order in which the signer appears on the document
			Role:        v6.ParticipantRole.Signer, // the role of the member
		},
		},
		SignatureType: v6.SignatureType.Esign,
		State:         v6.AgreementState.InProcess,
	})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(agreement)

	webhook, err := client.WebhookService.CreateWebhook(context.Background(), v6.CreateWebhookRequest{
		Name:                      "testing webhook",
		Scope:                     v6.Scope.Resource,
		State:                     "ACTIVE",
		WebhookSubscriptionEvents: []string{v6.WebhookSubscriptionEvent.AgreementAll},
		ResourceType:              v6.Resource.Agreement,
		ResourceId:                agreement.Id,
		WebhookUrlInfo:            v6.WebhookUrlInfo{Url: ""},
		WebhookConditionalParams: v6.WebhookConditionalParams{WebhookAgreementEvents: &v6.WebhookAgreementEvents{
			IncludeDetailedInfo:     true,
			IncludeDocumentsInfo:    true,
			IncludeParticipantsInfo: true,
			IncludeSignedDocuments:  true,
		}},
	})
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(webhook)
}
