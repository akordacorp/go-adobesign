package main

import (
	"gitlab.com/akordacorp/go-adobesign/adobesign/v6"
)

func main() {
	params := v6.Oauth2Params{
		ClientId:     "YOUR_CLIENT_ID",
		ClientSecret: "YOUR_CLIENT_SECRET",
		Scopes:       []string{"user_login:self", "agreement_send:account"},
		BaseUrl:      "YOUR_BASE_URL (example: secure.na1.adobesign.com)",
		RedirectUri:  "YOUR_REDIRECT_URI",
	}
	_ = v6.NewOauth2Client(params)
}
