ARCH := amd64
PLATFORMS := linux darwin
os = $(word 1, $@)
PKG_LIST := $(shell go list ./... | grep -v 'cache\|docs\|mocks\|examples\|cmd\|gen')

# Generate mocks for the project
mock:
	@echo "installing mockery..."
	@go install github.com/vektra/mockery/v2@latest
	@rm -rf ./db/mocks
	@mockery --dir ./db --recursive --all --keeptree --output ./db/mocks
	@mockery --dir ./client/artifacts --recursive --all --keeptree --output ./client/artifacts/mocks


test-deps:
	@echo "installing test dependencies..."
	@go install github.com/axw/gocov/gocov@latest
	@go install github.com/AlekSi/gocov-xml@latest
	@go install github.com/matm/gocov-html/cmd/gocov-html@latest
	@mkdir -p test-artifacts/coverage

test: test-deps
	@mkdir -p test-artifacts/coverage
	@gocov test ./... -short -covermode=atomic -coverpkg=./... > test-artifacts/gocov.json
	@cat test-artifacts/gocov.json | gocov report
	@cat test-artifacts/gocov.json | gocov-xml > test-artifacts/coverage/coverage.xml
	@cat test-artifacts/gocov.json | gocov-html > test-artifacts/coverage/coverage.html

# Linter config from https://docs.gitlab.com/ee/development/go_guide/
lint:
	@echo "installing lint deps.."
	@wget -O- -nv https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s latest
	@golangci-lint run --skip-dirs-use-default --out-format code-climate | tee gl-lint-report.json | jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'

lint-local:
	@golangci-lint run --fix --skip-dirs-use-default --out-format code-climate | tee gl-lint-report.json | jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'

doc:
	@go install golang.org/x/tools/cmd/godoc@latest
	@godoc -http=:6060

clean:
	rm -rf test-artifacts
	docker compose down